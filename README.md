JSSOR NO-JQUERY   
https://www.jssor.com/ (Jssor.Slider 19.0)  
First and most important, this version of JSSOR IS NOT a replacement for the original and
doesn't include all available options.  
Also, keep in mind that the js file (jssor.slider.js) have been changed, so any updates of the slider will break it.

