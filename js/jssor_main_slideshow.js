/**
*   Global Space:
*       MiceAux
*
*
**/

var MiceAux = {};
MiceAux.caroucels = {}
//......
function _cons ( messg ) { console.log( messg ); }
//......
document.addEventListener('DOMContentLoaded', onLoadDOM ) 
function onLoadDOM ( messg )
{
		//=========================== JSSOR DEFINITIONS
        //=========================== JSSOR DEFINITIONS
        //=========================== JSSOR DEFINITIONS
        //=========================== JSSOR DEFINITIONS
    		
            //...... CAPTIONS TRANSITIONS
            var _CaptionTransitions = [];
                _CaptionTransitions["L"] = { $Duration: 800, x: 0.6, $Easing: { $Left: $JssorEasing$.$EaseInOutSine }, $Opacity: 2 };
                _CaptionTransitions["R"] = { $Duration: 800, x: -0.6, $Easing: { $Left: $JssorEasing$.$EaseInOutSine }, $Opacity: 2 };
                _CaptionTransitions["T"] = { $Duration: 800, y: 0.6, $Easing: { $Top: $JssorEasing$.$EaseInOutSine }, $Opacity: 2 };
                _CaptionTransitions["B"] = { $Duration: 800, y: -0.6, $Easing: { $Top: $JssorEasing$.$EaseInOutSine }, $Opacity: 2 };
                _CaptionTransitions["TL"] = { $Duration: 800, x: 0.6, y: 0.6, $Easing: { $Left: $JssorEasing$.$EaseInOutSine, $Top: $JssorEasing$.$EaseInOutSine }, $Opacity: 2 };
                _CaptionTransitions["TR"] = { $Duration: 800, x: -0.6, y: 0.6, $Easing: { $Left: $JssorEasing$.$EaseInOutSine, $Top: $JssorEasing$.$EaseInOutSine }, $Opacity: 2 };
                _CaptionTransitions["BL"] = { $Duration: 800, x: 0.6, y: -0.6, $Easing: { $Left: $JssorEasing$.$EaseInOutSine, $Top: $JssorEasing$.$EaseInOutSine }, $Opacity: 2 };
                _CaptionTransitions["BR"] = { $Duration: 800, x: -0.6, y: -0.6, $Easing: { $Left: $JssorEasing$.$EaseInOutSine, $Top: $JssorEasing$.$EaseInOutSine }, $Opacity: 2 };

                _CaptionTransitions["WAVE|L"] = { $Duration: 1500, x: 0.6, y: 0.3, $Easing: { $Left: $JssorEasing$.$EaseLinear, $Top: $JssorEasing$.$EaseInWave }, $Opacity: 2, $Round: { $Top: 2.5} };
                _CaptionTransitions["MCLIP|B"] = { $Duration: 600, $Clip: 8, $Move: true, $Easing: $JssorEasing$.$EaseOutExpo };
                _CaptionTransitions["ZMF|10"] = { $Duration: 900, $Zoom: 11, $Easing: { $Zoom: $JssorEasing$.$EaseOutQuad, $Opacity: $JssorEasing$.$EaseLinear }, $Opacity: 2 };
                   
                _CaptionTransitions["RTT|10"] = { $Duration: 900, $Zoom: 11, $Rotate: 1, $Easing: { $Zoom: $JssorEasing$.$EaseOutQuad, $Opacity: $JssorEasing$.$EaseLinear, $Rotate: $JssorEasing$.$EaseInExpo }, $Opacity: 2, $Round: { $Rotate: 0.8 } };
                _CaptionTransitions["RTT|2"] = { $Duration: 900, $Zoom: 3, $Rotate: 1, $Easing: { $Zoom: $JssorEasing$.$EaseInQuad, $Opacity: $JssorEasing$.$EaseLinear, $Rotate: $JssorEasing$.$EaseInQuad }, $Opacity: 2, $Round: { $Rotate: 0.5 } };
                _CaptionTransitions["RTTL|BR"] = { $Duration: 900, x: -0.6, y: -0.6, $Zoom: 11, $Rotate: 1, $Easing: { $Left: $JssorEasing$.$EaseInCubic, $Top: $JssorEasing$.$EaseInCubic, $Zoom: $JssorEasing$.$EaseInCubic, $Opacity: $JssorEasing$.$EaseLinear, $Rotate: $JssorEasing$.$EaseInCubic }, $Opacity: 2, $Round: { $Rotate: 0.8 } };
                _CaptionTransitions["CLIP|LR"] = { $Duration: 900, $Clip: 15, $Easing: { $Clip: $JssorEasing$.$EaseInOutCubic }, $Opacity: 2 };
                _CaptionTransitions["MCLIP|L"] = { $Duration: 900, $Clip: 1, $Move: true, $Easing: { $Clip: $JssorEasing$.$EaseInOutCubic } };
                _CaptionTransitions["MCLIP|R"] = { $Duration: 900, $Clip: 2, $Move: true, $Easing: { $Clip: $JssorEasing$.$EaseInOutCubic } };
        
            //...... SLIDE TRANSITIONS 
            //MORE OPTIONS: http://www.jssor.com/development/tool-slideshow-transition-viewer.html
            var _SlideshowTransitions = [
                        //................ Fade
                        {$Duration:1200,$Opacity:2},
                        //................ Slide Down 
                        {$Duration:500,y:1,$Easing:$JssorEasing$.$EaseInQuad},
                        //................ Slide Right 
                        {$Duration:400,x:1,$Easing:$JssorEasing$.$EaseInQuad},
                        //................ Switch
                        {$Duration:1400,x:0.25,$Zoom:1.5,$Easing:{$Left:$JssorEasing$.$EaseInWave,$Zoom:$JssorEasing$.$EaseInSine},$Opacity:2,$ZIndex:-10,$Brother:{$Duration:1400,x:-0.25,$Zoom:1.5,$Easing:{$Left:$JssorEasing$.$EaseInWave,$Zoom:$JssorEasing$.$EaseInSine},$Opacity:2,$ZIndex:-10}
                        },
                        //................ Return LR
                        {$Duration:1200,x:1,$Delay:40,$Cols:6,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Easing:{$Left:$JssorEasing$.$EaseInOutQuart,$Opacity:$JssorEasing$.$EaseLinear},$Opacity:2,$ZIndex:-10,$Brother:{$Duration:1200,x:1,$Delay:40,$Cols:6,$Formation:$JssorSlideshowFormations$.$FormationStraight,$Easing:{$Top:$JssorEasing$.$EaseInOutQuart,$Opacity:$JssorEasing$.$EaseLinear},$Opacity:2,$ZIndex:-10,$Shift:-100}
                        },
                        //................ Shift TB 
                        {$Duration:1200,y:1,$Easing:{$Top:$JssorEasing$.$EaseInOutQuart,$Opacity:$JssorEasing$.$EaseLinear},$Opacity:2,$Brother:{$Duration:1200,y:-1,$Easing:{$Top:$JssorEasing$.$EaseInOutQuart,$Opacity:$JssorEasing$.$EaseLinear},$Opacity:2}
                        },
                        //................ Chess Replace TB
                        {$Duration:1600,x:1,$Rows:2,$ChessMode:{$Row:3},$Easing:{$Left:$JssorEasing$.$EaseInOutQuart,$Opacity:$JssorEasing$.$EaseLinear},$Opacity:2,$Brother:{$Duration:1600,x:-1,$Rows:2,$ChessMode:{$Row:3},$Easing:{$Left:$JssorEasing$.$EaseInOutQuart,$Opacity:$JssorEasing$.$EaseLinear},$Opacity:2}
                        },
                        //................ Zoom+ in
                        {$Duration:1000,$Zoom:11,$Easing:{$Zoom:$JssorEasing$.$EaseInCubic,$Opacity:$JssorEasing$.$EaseOutQuad},$Opacity:2
                        },
                        //................ Rotate HDouble+ in
                        {$Duration:1200,x:2,y:1,$Cols:2,$Zoom:11,$Rotate:1,$ChessMode:{$Column:15},$Easing:{$Left:$JssorEasing$.$EaseInCubic,$Top:$JssorEasing$.$EaseInCubic,$Zoom:$JssorEasing$.$EaseInCubic,$Opacity:$JssorEasing$.$EaseOutQuad,$Rotate:$JssorEasing$.$EaseInCubic},$Assembly:2049,$Opacity:2,$Round:{$Rotate:0.7}
                        },
                        //................ (Left to Right)
                        { $Duration:800,x:1,$Easing:{$Left:$JssorEasing$.$EaseInOutQuart,$Opacity:$JssorEasing$.$EaseLinear},$Opacity:2,$Brother:{$Duration:1200,x:-1,$Easing:{$Left:$JssorEasing$.$EaseInOutQuart,$Opacity:$JssorEasing$.$EaseLinear},$Opacity:2}
                        },
                        //................Flutter Outside in Wind
                        {$Duration:1800,x:1,y:0.2,$Delay:30,$Cols:10,$Rows:5,$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$Reverse:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Easing:{$Left:$JssorEasing$.$EaseInOutSine,$Top:$JssorEasing$.$EaseOutWave,$Clip:$JssorEasing$.$EaseInOutQuad},$Assembly:2050,$Outside:true,$Round:{$Top:1.3}
                        },
                        //................ (squares wave animation)
                        { $Duration:600,   
                            x:0.2, 
                            y:-0.1, 
                            $Delay:20,
                            $Cols:8,$Rows:4,
                            $Clip:15,
                            $During:{
                                        $Left:[0.3,0.7],
                                        $Top:[0.3,0.7]
                                    },
                                    $Formation:$JssorSlideshowFormations$.$FormationStraightStairs,
                                    $Easing:{   $Left:$JssorEasing$.$EaseInWave,
                                                $Top:$JssorEasing$.$EaseInWave,
                                                $Clip:$JssorEasing$.$EaseOutQuad
                                            },
                                    $Assembly:260,
                                    $Outside:true,
                                    $Round:{    $Left:1.3,
                                                $Top:2.5
                                            }
                        }       
            ]; 
            //............... RESPONSIVE
            MiceAux.caroucels.ScaleSlider = function(qualSlider, id_slider)         
            {
                    var parentWidth = qualSlider.$Elmt.parentNode.clientWidth;
                    
                    var getStyleElem = document.querySelector(id_slider);
                    var getStyle1 = parseInt(window.getComputedStyle(getStyleElem)['max-width']);

                    if (parentWidth) 
                    {
                        qualSlider.$ScaleWidth(Math.min(parentWidth, getStyle1));
                    
                    } else {
                        $Jssor$.$Delay(ScaleSlider, 30);
                    } 
            };
            //............... RESPONSIVE CODE BEGIN
            MiceAux.caroucels.initRWD = function (qualSlide)
            {
                //...............
                var name        =  qualSlide.name;
                var sliderID    =  qualSlide.slider_holder_id;
                //...............
                MiceAux.caroucels.ScaleSlider(  name,       
                                                sliderID   
                                        );
                //............... 
                $Jssor$.$AddEvent(window, "load",                                                   function(valor, id_){ MiceAux.caroucels.ScaleSlider( name, sliderID) } ); 
                $Jssor$.$AddEvent(window, "resize",            $Jssor$.$WindowResizeFilter(window,  function(valor, id_){ MiceAux.caroucels.ScaleSlider( name, sliderID) }) );
                $Jssor$.$AddEvent(window, "orientationchange",                                      function(valor, id_){ MiceAux.caroucels.ScaleSlider( name, sliderID) }  );
            }
            //............... CONTADOR SLIDES
            MiceAux.caroucels.DisplayIndex = function (                 
                                        slideIndex, 
                                        fromIndex, 
                                        
                                        qualSlider,
                                        
                                        qualSlider_counter_id
                                        
                                    ) 
                {
                    //............ 
                    var sliderCounter = $Jssor$.$GetElement(qualSlider_counter_id);
                    $Jssor$.$InnerText(sliderCounter, slideIndex + 1 + " / " + qualSlider.$SlidesCount());
                    //............ 
                };
            //............... CONTADOR SLIDES
            MiceAux.caroucels.initContadorSlides = function (qualSlide)
            {
                var name = qualSlide.name;
                var slider = qualSlide.slider_holder_id;
                var sliderID = document.querySelector(slider + ' .slider-counters');
                
                name.$On($JssorSlider$.$EVT_PARK, 
                    function( 
                        slideIndex, fromIndex    
                    )
                    { 
                                                    
                        MiceAux.caroucels.DisplayIndex( 
                                                        slideIndex, fromIndex, 
                                                        name,
                                                        sliderID    
                                                    );
                    } 
                    ); //....$On(...
            } 
            //............... PROGRESS BAR    
            MiceAux.caroucels.UpdateProgress = function(                  
                                            slideIndex, 
                                            progress, 
                                            progressBegin, 
                                            idleBegin, 
                                            idleEnd, 
                                            progressEnd, 
                                            qualProgressBar
                                        ) 
            {
                    if (progressEnd > 0) 
                    {
                        var progressPercent = progress / progressEnd * 100 + "%";
                        qualProgressBar.style.width = progressPercent;
                    }
            }       
            //............... PROGRESS BAR
            MiceAux.caroucels.initProgressBar = function (qualSlide)
            {
                var name = qualSlide.name;
                var slider = qualSlide.slider_holder_id;
                var progressBar = document.querySelector(slider + ' .progress_bar_slideshow');

                name.$On($JssorSlider$.$EVT_PROGRESS_CHANGE, 
                    function( 
                        slideIndex, progress, progressBegin, idleBegin, idleEnd, progressEnd, 
                        valor 
                    )
                    { 
                        MiceAux.caroucels.UpdateProgress(
                            slideIndex, progress, progressBegin, idleBegin, idleEnd, progressEnd, 
                            progressBar 
                            )
                    } 
                    );//...$On(
            }
                               
        //=========================== JSSOR DEFINITIONS
        //=========================== JSSOR DEFINITIONS
        //=========================== JSSOR DEFINITIONS
        //=========================== JSSOR DEFINITIONS   END!




        












        //=========================== EXEMPLOS
        //=========================== EXEMPLOS
        //=========================== EXEMPLOS
        //=========================== EXEMPLOS 
            //=================SLIDE 1
            //=================SLIDE 1
            //=================SLIDE 1
            //=================SLIDE 1    
            
                //......    
                MiceAux.caroucels.caroucel1 = {
                    //......
                    num: "",
                    //...... https://www.jssor.com/development/reference-options.html
                    options:   {
                                    $FillMode: 2,                                       //[Optional] The way to fill image in slide, 
                                                                                        //0 stretch, 
                                                                                        //1 contain (keep aspect ratio and put all inside slide), 
                                                                                        //2 cover (keep aspect ratio and cover whole slide), 
                                                                                        //4 actual size, 
                                                                                        //5 contain for large image, actual size for small image, 
                                                                                        //default value is 0
                                    $AutoPlay: true, 
                                    $AutoPlayInterval: 2500,        //SET BY setTimer()
                                    $PauseOnHover: 1,                                 
                                    $DragOrientation: 1,
                                    $PlayOrientation: 1,
                                    //$Cols: 3,                               
                                    //$SlideWidth: 400,                                
                                    //$SlideSpacing: 10,

                                    $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
                                        $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
                                        $Transitions: _SlideshowTransitions,            //[Required] An array of slideshow transitions to play slideshow
                                        $TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
                                        $ShowLink: true                                 //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
                                    },

                                    $CaptionSliderOptions: 
                                    {                                               //[Optional] Options which specifies how to animate caption
                                        $Class: $JssorCaptionSlider$,                   //[Required] Class to create instance to animate caption
                                        $CaptionTransitions: _CaptionTransitions,       //[Required] An array of caption transitions to play caption, see caption transition section at jssor slideshow transition builder
                                        $PlayInMode: 1,                                 //[Optional] 0 None (no play), 1 Chain (goes after main slide), 3 Chain Flatten (goes after main slide and flatten all caption animations), default value is 1
                                        $PlayOutMode: 3                                 //[Optional] 0 None (no play), 1 Chain (goes before main slide), 3 Chain Flatten (goes before main slide and flatten all caption animations), default value is 1
                                    },
                                
                                    $ArrowNavigatorOptions:                         //[Optional] Options to specify and enable arrow navigator or not
                                    {                       
                                        $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
                                        $ChanceToShow: 2,       //SET BY setArrows()    //[Required] 0 Never, 1 Mouse Over, 2 Always
                                        $AutoCenter: 0,                                 //[Optional] Auto center arrows in parent container, 0 No, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                                        $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                                    },

                                    $BulletNavigatorOptions:                       //[Optional] Options to specify and enable navigator or not
                                    {                                
                                        $Class: $JssorBulletNavigator$,                 //[Required] Class to create navigator instance
                                        $ChanceToShow: 2,   //SET BY setBullets()       //[Required] 0 Never, 
                                                                                        //1 Mouse Over, 
                                                                                        //2 Always
                                        $ActionMode: 1,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
                                        $AutoCenter: 0,                                 //[Optional] Auto center navigator in parent container, 
                                                                                        //0 None, 
                                                                                        //1 Horizontal, 
                                                                                        //2 Vertical, 
                                                                                        //3 Both, 
                                                                                        //default value is 0
                                        $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                                        $Lanes: 0,                                      //[Optional] Specify lanes to arrange items, default value is 1
                                        $SpacingX: 0,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                                        $SpacingY: 0,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                                        $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                                    }
                    },
                    //......
                    name: "",
                    slider_holder_id:   "",      
                    slider_counter_id:  "",      
                    progress_bar_id:    "",
                    //......
                    setTimer : function(valor)
                    {
                        this.options.$AutoPlayInterval = valor;
                    }, 
                    //......
                    setNumber : function(valor)
                    {
                        this.num = valor;
                    }, 
                    //......
                    setArrows : function(valor)  //0 | 1 | 2
                    {
                        this.options.$ArrowNavigatorOptions.$ChanceToShow = valor;
                    },  
                    //......
                    setBullets : function(valor)  //0 | 1 | 2
                    {
                        this.options.$BulletNavigatorOptions.$ChanceToShow = valor;
                    },   
                    //......
                    setName : function()
                    {
                        var _options = this.options;
                        var slider = ('slider' + this.num + '_container');
                        
                        this.name = new $JssorSlider$(   slider, _options );
                    },
                    //......
                    set_slider_holder_id: function()
                    {
                        this.slider_holder_id = '#slider' + this.num + '_container';
                    },
                    //......
                    init: function(objOptions)
                    {
                        this.setNumber(objOptions.numID);
                        this.setTimer(objOptions.time);
                        this.setArrows(objOptions.arrows);
                        this.setBullets(objOptions.bullets);
                        this.setName();
                        this.set_slider_holder_id();     //ID 
                        
                        //......
                        if(objOptions.onStartSlideAnimation !== false)
                        {
                            //console.log('objOptions.onStartSlideAnimation !== false)!!!!!!!!!');
                            var callBackFuncStart = objOptions.onStartSlideAnimation;
                            this.name.$On(   $JssorSlider$.$EVT_SLIDESHOW_START,
                                            function(slideIndex,fromIndex)
                                            { 
                                                callBackFuncStart(slideIndex,fromIndex);
                                            }  
                                        );
                        }   
                        //......
                        if(objOptions.onEndSlideAnimation !== false)
                        {
                            //console.log('objOptions.onEndSlideAnimation !== false)!!!!!!!!!');
                            var callBackFuncEnd = objOptions.onEndSlideAnimation;
                            this.name.$On(   $JssorSlider$.$EVT_SLIDESHOW_END,
                                            function(slideIndex,fromIndex)
                                            { 
                                                callBackFuncEnd(slideIndex,fromIndex);
                                            }  
                                        );
                        }   
                        //......
                        if(objOptions.responsive === true)
                        {
                            MiceAux.caroucels.initRWD(this);
                        }
                        //......
                        if(objOptions.progressBar === true)
                        {
                            MiceAux.caroucels.initProgressBar(this); 
                        }else{
                            var slider = document.querySelector(this.slider_holder_id + " .progress_bar_slideshow");
                            slider.style.display = "none";
                        }
                        //......
                        if(objOptions.countSlides === true)
                        {
                            MiceAux.caroucels.initContadorSlides (this);
                        }else{
                            var slider = document.querySelector(this.slider_holder_id + " .slider-counters");
                            slider.style.display = "none";
                        }
                        //......
                        if(objOptions.captions === false)
                        {
                            var caption_holder = document.querySelectorAll(this.slider_holder_id + " .caption_holder");
                            
                            [].forEach.call(caption_holder, function(item){
                                //console.log("item:::"+item);
                                item.style.display = "none";
                            })
                            
                        }
                        //......
                    }
                };
                //...... INIT ELEMENTS 
                MiceAux.caroucels.caroucel1.init({  numID: 1,   //USED IN HTML AND SASS FOR ID AND CLASS
                                                    time: 1500, //milliseconds
                                                    arrows: 2,  //(2: show | 1: show mouseover | 0: don't show)
                                                    bullets: 2,  //(2: show | 1: show mouseover | 0: don't show)
                                                    responsive: true,
                                                    progressBar: true,
                                                    countSlides: true,
                                                    captions: true,     //... or in SASS, .caption_holder { display: none; ... 
                                                                        //... or in HTML, remove all classes  .caption_holder 
                                                    onStartSlideAnimation: false,    //functionName | false
                                                    onEndSlideAnimation: false       //functionName | false
                });
                
                //...... EXAMPLE CALLBACK FUNCTIONS 
                function slideAnimStart1(slideIndex,fromIndex)
                {
                    _cons("1 EVT_SLIDESHOW_Start  SLIDEINDEX:::" + slideIndex);
                }                                       
                function slideAnimEnd1(slideIndex,fromIndex)
                {
                    _cons("1 EVT_SLIDESHOW_END  SLIDEINDEX:::" + slideIndex);
                }                                       
                //...... 
                                                        
            //=================SLIDE 1 
            //=================SLIDE 1 
            //=================SLIDE 1 
            //=================SLIDE 1   END! 

               
                
  
}